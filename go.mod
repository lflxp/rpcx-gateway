module gitee.com/lflxp/rpcx-gateway

go 1.14

require (
	github.com/devopsxp/gateway v0.0.0-20200804202858-b14662ee8643
	github.com/gin-gonic/gin v1.6.3
	github.com/julienschmidt/httprouter v1.2.0
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/rpcxio/rpcx-examples v1.1.6
	github.com/rpcxio/rpcx-gateway v0.0.0-20200521025828-a39934d3752d
	github.com/smallnest/rpcx v0.0.0-20200729031544-75f1e2894fdb
)
